import { useState } from "react";

//This script here is written by Katlin Kalde
const messages = [
  "Enroll a dance courses 🏫",
  "Practice & practice 👠",
  "Dance like Carmen 💃🏾",
];

function App() {
  return (
    <div>
      <Steps />
    </div>
  );
}

function Steps() {
  const [step, setStep] = useState(1);
  const [isOpen, setIsOpen] = useState(true);

  function handlePrevious() {
    // In case you need to update state based in current state:
    // setStep((s) => s - 1); gives the same value as setStep(step + 1);
    // better use the first.  (s = s + 1 vs. s + 1)
    step > 1 && setStep((s) => s - 1);
  }
  function handleNext() {
    step < 3 && setStep((s) => s + 1);
  }

  // Callback function: In case you need to update state based in current state:
  // This onClick={() => setIsOpen((is) => !is)
  // if not u may use: setIsOpen(!isOpen);
  return (
    <div>
      <button className="close" onClick={() => setIsOpen((is) => !is)}>
        &times;
      </button>

      {isOpen && (
        <div className="steps">
          <div className="numbers">
            <div className={step >= 1 ? "active" : ""}>1</div>
            <div className={step >= 2 ? "active" : ""}>2</div>
            <div className={step >= 3 ? "active" : ""}>3</div>
          </div>
          <p className="message">
            {step !== 0 && `Step ${step} : ${messages[step - 1]}`}
          </p>
          <div className="buttons">
            <button
              style={{ backgroundColor: "blue", color: "white" }}
              onClick={handlePrevious}
            >
              Previous
            </button>
            <button
              style={{ backgroundColor: "blue", color: "white" }}
              onClick={handleNext}
            >
              Next
            </button>
          </div>
        </div>
      )}
    </div>
  );
}

export default App;
