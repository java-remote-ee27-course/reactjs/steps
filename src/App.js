import { useState } from "react";

const messages = [
  "This is the first step",
  "Do not forget another",
  "The final step",
];

function App() {
  return (
    <div>
      <Steps />
      <Message step={4}>Reusable message 🦓</Message>
      <Message step={5}>Another reusable message 🐕‍🦺</Message>
    </div>
  );
}

function Steps() {
  const [step, setStep] = useState(1);
  const [isOpen, setIsOpen] = useState(true);

  function handlePrevious() {
    step > 1 && setStep((s) => s - 1);
  }
  function handleNext() {
    step < 3 && setStep((s) => s + 1);
  }

  function handleMessage() {
    alert(`Step number: ${step}`);
  }
  // Callback function: In case you need to update state based in current state:
  // This onClick={() => setIsOpen((is) => !is)
  // if not u may use: setIsOpen(!isOpen);
  return (
    <div>
      <button className="close" onClick={() => setIsOpen((is) => !is)}>
        &times;
      </button>

      {isOpen && (
        <div className="steps">
          <div className="numbers">
            <div className={step >= 1 ? "active" : ""}>1</div>
            <div className={step >= 2 ? "active" : ""}>2</div>
            <div className={step >= 3 ? "active" : ""}>3</div>
          </div>
          <Message step={step}>
            {messages[step - 1]}
            <div className="buttons">
              <Button
                textColor="white"
                background="orangered"
                onClick={handleMessage}
              >
                I am a reusable button
              </Button>
            </div>
          </Message>

          <div className="buttons">
            <Button
              textColor="white"
              background="limegreen"
              onClick={handlePrevious}
              width="7.5rem"
            >
              <span>👈</span> Previous
            </Button>
            <Button
              textColor="white"
              background="limegreen"
              onClick={handleNext}
              width="5.7rem"
            >
              Next <span>👉</span>
            </Button>
          </div>
        </div>
      )}
    </div>
  );
}

function Message({ step, children }) {
  return (
    <div className="message">
      <h3 className="header-three">Step {step}.</h3>
      <div className="message-div">{children}</div>
    </div>
  );
}

function Button({ textColor, background, onClick, width, children }) {
  return (
    <button
      style={{ color: textColor, backgroundColor: background, width: width }}
      onClick={onClick}
    >
      {children}
    </button>
  );
}

export default App;
