# Steps demo app

A demop app to show navigating forward and back in a list.
A demo of re-usable components.

![Steps2](./public/steps2.png)
![Steps1](./public/steps1.png)

By Katlin Kalde

For the reference of the app idea and the initial index.css please see: https://www.udemy.com/course/the-ultimate-react-course
